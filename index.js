const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const logger = require('morgan');
const cors = require('cors')
const apicache = require('apicache');
const yahooFinance = require('yahoo-finance');

let cache = apicache.middleware;
app.use(cache('5 minutes'));
app.use(cors())
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.get('/api/getQuote', async (req, res) => {
    try {
        let quote = req.query.quote
        if (quote) {
            await yahooFinance.quote({
                symbol: quote,
                modules: ['price', 'summaryDetail'] // see the docs for the full list
            }, function (err, quotes) {
                console.log(quotes)
                if (quotes) {
                    return res.status(200).send(quotes);
                } else {
                    return res.send("This Quote not found");
                }
            });
        } else {
            return res.status(500).send("Please input Quote");
        }
    } catch (error) {
        // res.send(error)
    }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.listen(3000, () => {
    console.log('Node App is running on port 3000');
})

module.exports = app;